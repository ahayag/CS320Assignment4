def isNumber(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

class Tokenizer:
    _validTokens = ["push", "pop", "add", "sub", "mul", "div", "mod", "skip", "save", "get"]
    def __init__(self):
        self._data = []
        self.invalidToken = []
    def Tokenize(self, string):
        newString = string.replace('\t', ' ')  #removes any tab character in middle of string. ex) "get\t3" -> "get 3"
        sp = newString.split(' ')  #split input string on space characer and store in list sp
        tokenList = [x for x in sp if len(x) > 0]  #trims list so tokenList will only contain tokens and no leading or trailing spaces
        for i in range(0, len(tokenList)):
            token = tokenList[i]
            if token in self._validTokens or isNumber(token):  #if token is valid, save inside tokenizer     
                                                               #else, raise value error
                self._data.append(token)
            else: 
                self.invalidToken.append(token)
                return "error"
    def GetTokens(self):  #return the list of valid tokens
        return self._data     
        
onePartCmds = ["pop", "add", "sub", "mul", "div", "mod", "skip"]
twoPartCmds = ["push", "save", "get"]

def IsOnePart(tokens):  #verifies that a single token is a one part command (pop, add, sub...)
    if tokens[0] in onePartCmds:
        return True
    else:
        return False

def isTwoPart(tokens): #verifies that the first token is a two part command and the second token is a number
    if tokens[0] in twoPartCmds and SecondIsNumber(tokens):
        return True
    else:
        return False

def SecondIsNumber(tokens):  #checks if the second token is a number
    try:
        int(tokens[1])
        return True
    except ValueError:
        return False

def Parse(tokens):  #function takes list of tokens and validates according to parse rules
    tokString = " ".join(tokens)  #convert tokens list into a string. ex) ["push", "7"] -> "push 7"
    if len(tokens) == 0 or len(tokens) > 2:
        return "error"
    elif len(tokens) == 1:
        if not IsOnePart(tokens):
            return "error"
        else:
            return tokString
    elif len(tokens) == 2:
        if not isTwoPart(tokens):
            return "error"
        else:
            return tokString