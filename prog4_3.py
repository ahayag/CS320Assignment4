import prog4_1
import prog4_2
import sys

def doTokenize(tokenList):  #tokenizes each string inside tokenList. ex) tokenizes tokenList = ["push","7","pop"]
    t = prog4_1.Tokenizer()
    for line in range(0, len(tokenList)):
        if t.Tokenize(tokenList[line]) == "error":
            raise ValueError("Error on line " + str(line+1) + ": " + "Unexpected token: " + t.invalidToken.pop())

def doParse(tokenList):  #parses each list inside tokenList. ex) parses tokenList = [ ["push","7"], ["pop"] ]
    for line in range(0, len(tokenList)):
        if prog4_1.Parse(tokenList[line]) == "error":
            tokString = " ".join(tokenList[line])
            raise ValueError("Parse error on line " + str(line+1) + ": " + tokString)

def doExecute(tokenList):
    sm = prog4_2.Stackmachine()
    while (sm.currentline < len(tokenList)):  
        if sm.currentline < 0:
            raise ValueError("Trying to execute invalid line: " + str(sm.currentline))
        returnVal = sm.Execute(tokenList[sm.currentline])  #execute the operation and store the return value here
        if returnVal != None:  
            print(returnVal)
        if returnVal == "error":
            tokString = " ".join(tokenList[sm.currentline])
            raise ValueError("Line " + str(sm.currentline+1) + ": '" + tokString + "' caused Invalid Memory Access")

def main():
    print("Assignment #4-3, Aaron Hayag, aaron_hayag@yahoo.com")
    with open(sys.argv[1], mode = 'r') as f:
        lines = [x.strip() for x in f.readlines()]
        doTokenize(lines)
        tokens = [x.split() for x in lines]
        doParse(tokens)
        doExecute(tokens)
        print("Program terminated correctly")
        
if __name__ == "__main__":
    main()