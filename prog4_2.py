def doOperation(str1, str2, operation): #takes 2 string numbers and performs an operation. returns result as string
    num1 = int(str1)
    num2 = int(str2)

    if operation == "add":
        result = num1 + num2
        return str(result)
    elif operation == "sub":
        result = num1 - num2
        return str(result)
    elif operation == "mul":
        result = num1 * num2
        return str(result)
    elif operation == "div":
        result = int(num1 / num2)
        return str(result)
    elif operation == "mod":
        result = int(num1 % num2)
        return str(result)
    
class Stackmachine:
    currentline = 0
    stackSize = 0
    def __init__(self):
        self._data = []
        self._memory = [None for x in range(0,10)]  #will be used for save and get operations
    def Execute(self,tokens):
        if "push" in tokens:
            self._data.append(tokens[1])
            self.stackSize += 1
            self.currentline += 1
        if "pop" in tokens:
            if self.stackSize == 0:
                return "error"
            else:
                self.stackSize -= 1
                self.currentline += 1
                return self._data.pop()
        if "add" in tokens:
            if self.stackSize < 2:  #error if stack is empty or has 1 value
                return "error"
            else:
                num1 = self._data.pop()  #pop first value
                num2 = self._data.pop()  #pop second value
                result = doOperation(num1, num2, tokens[0])  #add together
                self._data.append(result)  #push result
                self.stackSize -= 1
                self.currentline += 1
        if "sub" in tokens:
            if self.stackSize < 2:
                return "error"
            else:
                num1 = self._data.pop()
                num2 = self._data.pop()
                result = doOperation(num1, num2, tokens[0])
                self._data.append(result)
                self.stackSize -= 1
                self.currentline += 1
        if "mul" in tokens:
            if self.stackSize < 2:
                return "error"
            else:
                num1 = self._data.pop()
                num2 = self._data.pop()
                result = doOperation(num1, num2, tokens[0])
                self._data.append(result)
                self.stackSize -= 1
                self.currentline += 1
        if "div" in tokens:
            if self.stackSize < 2:
                return "error"
            else:
                num1 = self._data.pop()
                num2 = self._data.pop()
                result = doOperation(num1, num2, tokens[0])
                self._data.append(result)
                self.stackSize -= 1
                self.currentline += 1
        if "mod" in tokens:
            if self.stackSize < 2:
                return "error"
            else:
                num1 = self._data.pop()
                num2 = self._data.pop()
                result = doOperation(num1, num2, tokens[0])
                self._data.append(result)
                self.stackSize -= 1
                self.currentline += 1
        if "save" in tokens:  #pops one value and save that value in memory. saves up to 10 values starting from index 0 to 9
            if self.stackSize == 0:
                return "error"
            else:
                index = int(tokens[1])
                if index > 9:
                    return "error"  #error if trying to save to memory index 10 or higher
                self._memory[index] = self._data.pop()
                self.stackSize -= 1
                self.currentline += 1
        if "get" in tokens:  #get a previously saved value and push on the stack
            index = int(tokens[1])
            retrievedVal = self._memory[index]
            if retrievedVal == None:
                return "error"  #error if trying to get a value from empty memory index
            self._data.append(retrievedVal)
            self.stackSize += 1
            self.currentline += 1
        if "skip" in tokens:  #pops two values. if first value is 0, change currentline by the second value
            if self.stackSize < 2:
                return "error"
            val1 = int(self._data.pop())
            val2 = int(self._data.pop())
            if val1 == 0:
                self.currentline += (val2 + 1)
            else:
                self.currentline += 1
            self.stackSize -= 2