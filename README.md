Aaron Hayag aaron_hayag@yahoo.com

CS320 Assignment 4

prog4_1 is a Tokenizer and Parser implementation in python. Tokenize(str) is a function that takes an input string, tokenizes on the space
delimeter, and checks that each token is valid. Valid tokens are: push, pop, add, sub, mul, div, mod, save, get, skip, and any whole number digit.
The function will raise a value error if an invalid token is found. If all tokens are valid, The function returns the list of valid tokens. 
Parse(tokens) function takes a list of tokens and validates that the tokens adhere to the parsing rules. Per the parsing rules, 
tokens pop, add, sub, mul, div, and skip are one part commands while push #, save #, and get # are two part commands. Parser will raise a value error
if a token string violates the parsing rules. 

prog4_2 is an implementation of a StackMachine class with an Execute(tokens) function, that executes each token operations, and a currentline property,
that increments whenever the Execute function is called. Execute function will raise a value error when trying to execute a token operation that can
not be performed. ex) calling pop when stack is empty

prog4_3 is the driver program. It reads the first command line argument as a file, tokenizes and parses each line in the file, (using the functions from
prog4_1) and finally executes the tokens in the file dictated by the StackMachine's currentline property. Any value returned by a token operation is printed
on its own line to STDOUT. Any tokenization error, parsing error, or execution error will raise a value error. 

Example execution:
    python3 prog4_3.py testfile.sm